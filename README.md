# Simple http server socket (golang)

### Description

Simple http server (and client), golang, tcp sockets. Done WITHOUT [http](https://golang.org/pkg/net/http/) package. If you want more canonical http golang server, go check http package.

### Usage
Two consoles:
1:
```
go run server.go
```
2:
```
go run client/client.go 127.0.0.1:7777
```
Also you can open http://localhost:7777 in browser

### Task (RU)
Разработать клиент-серверное приложение на основе сетевых сокетов и протокола TCP. Сервер должен выполнять передачу файла (текстового или бинарного) клиенту.

Лабораторная засчитывается на доп. баллы (как две работы) в случае, если TCP сервер расширен до http-сервера. При этом должны выполняться следующие условия:

 http сервер должен обрабатывать GET-запросы и предоставлять статическую информацию клиенту. 

Разработать http-клиент для проверки данного сервера с помощью GET-запросов. 

Желательно предусмотреть многопоточную обработку соединений. Потоки для соединений выделяются из пула потоков.