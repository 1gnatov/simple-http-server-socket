package main

import (
	"fmt"
	"net"
	"os"
)

func main() {
	name := "127.0.0.1"
	if len(os.Args) == 2 {
		name = os.Args[1]
	}
	addr := net.ParseIP(name)
	if addr == nil {
		fmt.Println("Invalid address")
	} else {
		fmt.Println("The address is ", addr.String())
	}
	os.Exit(0)
}
