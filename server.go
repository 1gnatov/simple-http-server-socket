package main

import (
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strings"
)

func main() {
	port := "7777"
	tcpAddr, err := net.ResolveTCPAddr("tcp4", ":"+port)
	checkError(err)
	listener, err := net.ListenTCP("tcp", tcpAddr)
	checkError(err)
	for {
		conn, err := listener.Accept()
		if err != nil {
			continue
		}
		go handleClient(conn) // multiple clients by goroutines
	}
}

func handleClient(conn net.Conn) {
	defer conn.Close()

	buff := make([]byte, 1024)

	n, err := conn.Read(buff)
	checkError(err)
	fmt.Println("Number of bytes:", n)
	clientMsg := string(buff[0:n])
	fmt.Print(clientMsg)
	clientMsgFields := strings.Fields(clientMsg)
	httpMethod := clientMsgFields[0]
	if httpMethod != "GET" {
		conn.Write([]byte("HTTP/1.0 405 Method no allowed\n\r\n\r"))
	} else {
		path := clientMsgFields[1]
		pathToRead := "static" + path
		if strings.HasSuffix(pathToRead, "/") {
			pathToRead += "index.html"
		}
		file, err := readFile(pathToRead)
		if err != nil {
			conn.Write([]byte("HTTP/1.0 404 Not found\n\r\n\r"))
		} else {
			conn.Write([]byte("HTTP/1.0 200 OK\n\r\n\r"))
			conn.Write(file)
		}

	}
}

func readFile(path string) ([]byte, error) {
	data, error := ioutil.ReadFile(path)
	return data, error
}

func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
